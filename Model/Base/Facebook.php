<?php


class Zrt_Model_Base_Facebook
        implements Zrt_Model_Base_Interface
    {

    // This class will behave in a similar fashion to Zrt_Model_Base_DbTable
    // and reference a document, like the latter references a Zend_Db_Table_Row

    /**
     * The adapter to use for this connection.
     *
     * @var Zrt_Db_Document_Adapter_Facebook
     */
    protected $_adapter;

    /**
     * The adapter to use if it hasn't been set.
     *
     * @var unknown_type
     */
    protected static $_defaultAdapter = null;

    /**
     * The fields that are present in the underlying representation of this model.
     *
     * @var array
     */
    protected $_fields = null;


    public function __construct( array $config = array( ) )
        {
        $this->_adapter = isset( $config['adapter'] ) ? $config['adapter'] : self::getDefaultAdapter();


        }


    public static function setDefaultAdapter( $adapter )
        {
        self::$_defaultAdapter = $adapter;


        }


    public static function getDefaultAdapter()
        {
        if ( null === self::$_defaultAdapter )
            {
            self::setDefaultAdapter( Zrt_Db::getAdapter( 'facebook' ) );
            }
        return self::$_defaultAdapter;


        }


    /**
     * Finds records based on identifiers.
     *
     * @param array|string $identifiers.  The identifier of the record to find or a view definition.
     * @return Zrt_Db_Document_CouchSet
     */
    public function find()
        {
        $arguments = func_get_args();
        $identifiers = $arguments[0];

        if ( !is_array( $identifiers ) )
            {
            $identifiers = array( $identifiers );
            }

        return $this->getAdapter()->find( $identifiers );


        }


    /**
     * Returns a set of records from a view based on the supplied parameters.
     *
     * @throws Zrt_Exception
     * @return Zrt_Db_Document_FacebookSet
     */
    public function fetchAll()
        {
        $arguments = func_get_args();
        $view = $arguments[0];
        if ( !$view instanceof Zrt_Db_Document_View )
            {
            throw new Zrt_Exception( "Must supply a valid view." );
            }
        return $this->getAdapter()->view( $view );


        }


    /**
     * Deletes from permanent storage, based on the supplied query.
     * @param array|string $where
     */
    public function delete( $where )
        {
        throw new Zrt_Exception( "Not implemented" );


        }


    public function createRecord( array $data = array( ) )
        {
        throw new Zrt_Exception( "Not implemented" );


        }


    /**
     * Returns the fields that represent this model on Facebook.
     *
     * @return array
     */
    public function getFields()
        {
        return $this->_fields;


        }


    /**
     * Gets the adapter for this base.
     *
     * @return Zrt_Db_Document_Adapter_Couch
     */
    public function getAdapter()
        {
        return $this->_adapter;


        }


    }


?>