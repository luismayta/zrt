<?php

/**
 * Zrt PHP Library
 *
 * @category Zrt
 * @package Zrt_Cli
 * @copyright Copyright (c) 2008-2010 Jamie Talbot (http://jamietalbot.com)
 * @version $Id: Exception.php 69 2010-09-08 12:32:03Z jamie $
 */


/**
 * Cli specific exception base.
 *
 * @category Zrt
 * @package Zrt_Cli
 */
class Zrt_Cli_Exception
        extends Exception
    {
    
    }