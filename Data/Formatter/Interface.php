<?php

/**
 * Zrt PHP Library
 *
 * @category Zrt
 * @package Zrt_Data
 * @copyright Copyright (c) 2008-2010 Jamie Talbot (http://jamietalbot.com)
 * @version $Id: Interface.php 69 2010-09-08 12:32:03Z jamie $
 */


/**
 * Defines an interface to format data objects
 *
 * @category Zrt
 * @package Zrt_Data
 */
interface Zrt_Data_Formatter_Interface
    {


    public static function format( Zrt_Data_Interface $object );

    }


?>