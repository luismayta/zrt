<?php


class Zrt_Controller_Action
        extends Zend_Controller_Action
    {

    protected $URL;
    protected $_translate;
    protected $Zrt;


    public function __call( $method , $args )
        {
        $this->_redirect( "/default/index/index" );
        }


    public function init()
        {
        $this->logger = new Zend_Log();
        $writer = new Zend_Log_Writer_Stream(
                        APPLICATION_PATH . '/../logs/log.txt'
                );
        $this->logger->addWriter( $writer );

        $this->view->messages = $this->_helper->flashMessenger->getMessages();

        $this->URL =
                $this->getRequest()->getModuleName()
                . '/' . $this->getRequest()->getControllerName();

        /* agregando traduccion en Controller */
        $this->_translate = Zend_Registry::get( 'Zend_Translate' );
        /* fin de Traduccion */
        $this->Zrt = new Zend_Session_Namespace( 'Zrt' );
        }


    public function preDispatch()
        {
        parent::preDispatch();
        }


    }